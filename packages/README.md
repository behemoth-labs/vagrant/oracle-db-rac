# Binary/Source Code Packages

RPM files are stored in `./rpms` directory, other compressed files, stored in `./tars` directory. Make sure to create these paths and have the required files in them.

```
mkdir -p ./rpms ./tars
```

This document will cover all the required files and how to get them. Some of the files are behind paywall/registration/eula etc, so it will require manual steps in order to get the files.

## Oracle

In order to download Oracle's stuff you'll need an account at Oracle. As you can't
just download stuff with `curl` or `wget`, you will need to manually download and
place the files in the `./rpms` folder.

Download URL: https://www.oracle.com/database/technologies/oracle19c-linux-downloads.html

Expected file `./rpms/oracle-database-ee-19c-1.0-1.x86_64.rpm`

### Preinstall

A preinstall package is required before the RPM itself can be installed.

```
curl "https://public-yum.oracle.com/repo/OracleLinux/OL8/appstream/x86_64/getPackage/oracle-database-preinstall-19c-1.0-2.el8.x86_64.rpm" \
  -o "./rpms/oracle-database-preinstall-19c-1.0-2.el8.x86_64.rpm"
```

*Pre-install package isn't a must, but it reduce the manual work require in order to run Oracle DB.*

### ASM

ASM are Oracle's packages to manage and monitor storage, it's not a must, but nice to have.

```
curl "https://public-yum.oracle.com/repo/OracleLinux/OL8/addons/x86_64/getPackage/oracleasm-support-2.1.12-1.el8.x86_64.rpm" \
  -o "./rpms/oracleasm-support-2.1.12-1.el8.x86_64.rpm"

curl "https://download.oracle.com/otn_software/asmlib/oracleasmlib-2.0.17-1.el8.x86_64.rpm" \
  -o "./rpms/oracleasmlib-2.0.17-1.el8.x86_64.rpm"
```

URLS:
  - https://access.redhat.com/solutions/5396481
    - https://www.oracle.com/linux/downloads/linux-asmlib-v8-downloads.html
    - https://public-yum.oracle.com/repo/OracleLinux/OL8/addons/x86_64/index.html
