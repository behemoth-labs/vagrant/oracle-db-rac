# Oracel Database Lab

This lab is WIP. Still not working.

TBD ...

## Box

This lab is using a custom Box, based on Red Hat Enterprise Linux 8.6. The file `rhel8_6.json` is referring to that box, so make sure you have the box and the json file. Each person who runs this lab will have a different content in `rhel8_6.json`, according to the location, hash signature, etc.

Example:

```json
{
    "name": "rhel8_6",
    "description": "Red Hat Enterprise Linux 8.6",
    "versions": [{
        "version": "8.6-4",
        "providers": [{
            "name": "libvirt",
            "url": "file:///home/itzhak/Downloads/BOXes/rhel8.6-4.box",
            "checksum": "d230d3e15360ca14d13050252553f08d00f6aa58a831acddbb69b26abfb9ca46",
            "checksum_type": "sha256"
        }]
    }]
} 
```
