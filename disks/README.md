# Disks

Shared disks.

## Creation

```
qemu-img create -f qcow2 ./disks/test.qcow2 5G
```
