# -*- mode: ruby -*-
# vi: set ft=ruby :
require_relative 'vagrant_credentials.rb'
include Secrets

servers=[
  {
    :hostname => "oracle-db-1",
    :ip => "192.168.33.61",
    :box => "rhel8_6",
    :box_json => "rhel8_6.json",
    :ram => 8200,
    :cpu => 4,
    :disk_size => 40
  }
#,
#   {
#     :hostname => "oracle-db-2",
#     :ip => "192.168.33.62",
#     :box => "rhel8_6",
#     :ram => 8200,
#     :cpu => 4,
#     :disk_size => 40
#   }
]

Vagrant.configure("2") do |config|
    # Resize ens5's mtu to '1392'.
    # This is an internal issue due to WireGuard VPN.
    config.vm.provision "shell",
        run: "always",
        inline: "sudo ip link set dev ens6 mtu 1392"

    # Copy 'packages.tar.gz' file, this is a bundle of all required files
    # We should consider to break this file in the future. But not now.
    config.vm.provision "file", source: "files/packages.tar.gz", destination: "/tmp/packages.tar.gz"

    # Oracle DB RPM
    config.vm.provision "file", source: "packages/rpms/oracleasm-support-2.1.12-1.el8.x86_64.rpm", destination: "/tmp/oracleasm-support-2.1.12-1.el8.x86_64.rpm"
    config.vm.provision "file", source: "packages/rpms/oracleasmlib-2.0.17-1.el8.x86_64.rpm", destination: "/tmp/oracleasmlib-2.0.17-1.el8.x86_64.rpm"
    config.vm.provision "file", source: "packages/rpms/oracle-database-preinstall-19c-1.0-2.el8.x86_64.rpm", destination: "/tmp/oracle-database-preinstall-19c-1.0-2.el8.x86_64.rpm"
    config.vm.provision "file", source: "packages/rpms/oracle-database-ee-19c-1.0-1.x86_64.rpm", destination: "/tmp/oracle-database-ee-19c-1.0-1.x86_64.rpm"

    # Initializing system
    config.vm.provision "shell", path: "scripts/init.sh", privileged: true, env: {"RHEL_SUB_PASS" => Secrets::RHEL_SUB_PASS, "RHEL_SUB_USER" => Secrets::RHEL_SUB_USER }
    # Installing Oracle DB RPM
    config.vm.provision "shell", path: "scripts/install_oracle.sh", privileged: true

    # Have to disable 'rsync', as it will try to install packages
    # and fail before the route was corrected.
    config.nfs.verify_installed = false
    config.vm.synced_folder './sync', '/vagrant', type: 'rsync', disabled: true

    # Don't inject pub key
    config.ssh.insert_key = false
    config.ssh.username = "vagrant"

    servers.each do |machine|
        config.vm.define machine[:hostname] do |node|
            config.vm.box = machine[:box]
            config.vm.box_url = [ machine[:box_json] ]
            node.vm.hostname = machine[:hostname]

            # Host-to-host network
            node.vm.network :private_network,
                :ip => machine[:ip],
                :libvirt__domain_name => "redhat.local"

            node.vm.provider :libvirt do |lv|
                lv.machine_virtual_size = machine[:disk_size]
                lv.qemu_use_session = false
                lv.cpus = machine[:cpu]
                lv.driver = "kvm"
                lv.memory = machine[:ram]
                lv.nic_model_type = "e1000"
                lv.nested = true

                # Additional Disk
                lv.storage :file, :size => '5G', :path => 'test.raw', :allow_existing => true, :shareable => true, :type => 'raw'
                # lv.storage :file, :size => '20G', :path => 'my_shared_disk.img', :allow_existing => true, :shareable => true, :type => 'raw'
                # lv.storage :file, :size => '5G', :path => 'test.qcow2', :allow_existing => true, :shareable => true, :type => 'qcow2'
            end
        end
    end

    # config.trigger.before :destroy do |trigger|
    #     trigger.name = "Making sure to unregister from RHEL"
    #     trigger.run_remote = { inline: "sudo subscription-manager unregister" }
    #     trigger.on_error = :halt
    # end
end
