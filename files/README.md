# Quick Guide

Here we'll find different files that are required during setup of the instance.

## Packaging Everything

Create a tar file with the following command

```
tar -cvz --exclude='*.tar.gz' --exclude='README.md' -f packages.tar.gz *
```
