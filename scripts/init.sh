#!/bin/bash
#
# Requires 'packages.tar.gz' file to be placed under '/tmp'.

# "Disable" SELinux
#sudo sed -i "/^[^#]*SELINUX=enforcing/c\SELINUX=permissive" /etc/selinux/config
#sudo setenforce 0
# Already disabled on the box level

# Update disk size (grow)
growpart /dev/vda 2
lvextend -l +100%FREE /dev/mapper/rhel-root
xfs_growfs /dev/mapper/rhel-root
df -h

# Subscribing to RHEL
echo 'Subscribing with RHEL...'
sudo subscription-manager register --username ${RHEL_SUB_USER} --password ${RHEL_SUB_PASS} \
  --auto-attach

# Waiting for license to proporgate
echo 'Sleeping for 30 seconds...'
sleep 30

# Extracting files from 'packages.tar.gz'.
mkdir -p /tmp/packages-extracted
tar -xvf /tmp/packages.tar.gz -C /tmp/packages-extracted
