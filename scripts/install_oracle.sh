#!/bin/bash

INTERNAL_DEVICE='ens7'

# Additional Packages
yum install -y \
  jq \
  net-tools \
  chronyd

systemctl enable --now chronyd

# Getting local IP address
INTERNAL_IP_ADDR=$(ip -json address show dev ${INTERNAL_DEVICE}|jq -r '.[]|.addr_info[]|select(.family == "inet")|.local')
INTERNAL_HOSTNAME=$(hostname)

echo "${INTERNAL_IP_ADDR} ${INTERNAL_HOSTNAME}" >> /etc/hosts # Oracle `configure` requires an association of an IP address with the hostname.

yum localinstall -y \
  /tmp/oracleasm-support-2.1.12-1.el8.x86_64.rpm \
  /tmp/oracleasmlib-2.0.17-1.el8.x86_64.rpm \
  /tmp/oracle-database-preinstall-19c-1.0-2.el8.x86_64.rpm \
  /tmp/oracle-database-ee-19c-1.0-1.x86_64.rpm

# Users and groups for grid
groupadd -g 5004 asmadmin
groupadd -g 5005 asmdba
groupadd -g 5006 asmoper
useradd -u 5010 -g oinstall -G asmadmin,asmdba,asmoper,dba grid

# Limits for grid user
cp -a /etc/security/limits.d/oracle-database-preinstall-19c.conf /etc/security/limits.d/grid.conf
sed -i 's/oracle/\grid/g' /etc/security/limits.d/grid.conf

# Directory Structure
mkdir -p /home/oracle
chown oracle: /home/oracle

mkdir -p /u01/app/grid
mkdir -p /u01/app/19c/gridhome_1
mkdir -p /u01/app/oraInventory
mkdir -p /u01/LATEST_PATCH
mkdir -p /u02/app/oracle/product/19c/dbhome_1

chown -R grid:oinstall /u01
chown -R oracle:oinstall /u02
chmod -R 0755 /u01 /u02

# bashrc (grid)
cat <<EOT > /home/grid/.bashrc
# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
export TMP=/tmp
export TMPDIR=$TMP
export ORACLE_BASE=/u01/app/grid
export ORACLE_HOME=/u01/app/19c/gridhome_1
export ORACLE_SID=+ASM1
export ORACLE_TERM=xterm
export PATH=$PATH:$ORACLE_HOME/bin
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib
export CLASSPATH=$ORACLE_HOME/JRE:$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib
EOT
chown grid: /home/grid/.bashrc

# bashrc (oracle)
cat <<EOT > /home/oracle/.bashrc
# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
export TMP=/tmp
export TMPDIR=$TMP
export ORACLE_BASE=/u02/app/oracle
export ORACLE_HOME=/u02/app/oracle/product/19c/gridhome_1
export ORACLE_SID=prod1
export ORACLE_TERM=xterm
export PATH=$PATH:$ORACLE_HOME/bin
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib
export CLASSPATH=$ORACLE_HOME/JRE:$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib
EOT
chown grid: /home/oracle/.bashrc
